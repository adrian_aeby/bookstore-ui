import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {CatalogComponent} from "./catalog/catalog.component";
import {BookDetailsComponent} from "./catalog/book-details.component";
import {BookListComponent} from "./catalog/book-list.component";
import {CatalogService} from "./services/catalog-service";
import {HomeComponent} from "./core/home.component";
import { RouterModule} from "@angular/router";
import {AppComponent} from "./core/app.component";

import {routes} from "./core/routes";
import {CacheService} from "./services/cache-service";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {LoginComponent} from "./forms/login.component";
import {RegistrationComponent} from "./forms/registration.component";
import {AccountComponent} from "./forms/account.component";
import {ChangePasswordComponent} from "./forms/change-password.component";
import {HttpModule} from "@angular/http";
import {RegistrationService} from "./services/registration.service";
import {CustomerService} from "./services/customer.service";


@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		RouterModule.forRoot(routes),
		HttpModule
	],
	providers: [
		CatalogService,
		CacheService,
		RegistrationService,
		CustomerService
	],
	declarations: [
		AppComponent,
		CatalogComponent,
		BookDetailsComponent,
		BookListComponent,
		HomeComponent,
		ShoppingCartComponent,
		LoginComponent,
		RegistrationComponent,
		AccountComponent,
		ChangePasswordComponent
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
