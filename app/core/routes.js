"use strict";
var catalog_component_1 = require('../catalog/catalog.component');
var home_component_1 = require('./home.component');
var book_details_component_1 = require('../catalog/book-details.component');
var shopping_cart_component_1 = require('../shopping-cart/shopping-cart.component');
var login_component_1 = require('../forms/login.component');
var registration_component_1 = require('../forms/registration.component');
var change_password_component_1 = require('../forms/change-password.component');
exports.routes = [
    { path: 'catalog', component: catalog_component_1.CatalogComponent },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'book-details/:isbn', component: book_details_component_1.BookDetailsComponent },
    { path: 'shopping-cart', component: shopping_cart_component_1.ShoppingCartComponent },
    { path: 'registration', component: registration_component_1.RegistrationComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'change-password', component: change_password_component_1.ChangePasswordComponent },
    { path: '**', component: home_component_1.HomeComponent }
];
//# sourceMappingURL=routes.js.map