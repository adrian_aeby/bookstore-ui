export class BookInfo {
    constructor(
        readonly isbn: string,
        readonly title: string,
        readonly authors: string,
        readonly price: number) {
    }
}