"use strict";
var book_1 = require("./book");
exports.BOOK_DATA = [
    new book_1.Book("143024626X", "Beginning Java EE 7 (Expert Voice in Java)", "Antonio Goncalves", "Apress", 2013, book_1.BookBinding.PAPERBACK, 608, 49.99),
    new book_1.Book("1449370179", "Java EE 7 Essentials", "Arun Gupta", "O'Reilly Media", 2013, book_1.BookBinding.PAPERBACK, 362, 49.99),
    new book_1.Book("0071837345", "Java EE 7: The Big Picture", "Dr. Danny Coward", "McGraw-Hill Osborne Media", 2014, book_1.BookBinding.PAPERBACK, 512, 50.00),
    new book_1.Book("1300149310", "Real World Java EE Patterns-Rethinking Best Practices", "Adam Bien", "lulu.com", 2012, book_1.BookBinding.PAPERBACK, 432, 51.91),
    new book_1.Book("0596158025", "Enterprise JavaBeans 3.1", "Andrew Lee Rubinger, Bill Burke", "O'Reilly Media", 2010, book_1.BookBinding.PAPERBACK, 766, 54.99),
    new book_1.Book("1935182994", "EJB 3 in Action", "Debu Panda, Reza Rahman, Ryan Cuprak, Michael Remijan", "Manning Publications", 2014, book_1.BookBinding.PAPERBACK, 560, 54.99),
    new book_1.Book("1430246928", "Beginning EJB 3, Java EE, 7th Edition", "Jonathan Wetherbee, Raghu Kodali, Chirag Rathod, Peter Zadrozny", "Apress", 2013, book_1.BookBinding.PAPERBACK, 452, 49.99),
    new book_1.Book("1430249269", "Pro JPA 2 (Expert's Voice in Java)", "Mike Keith, Merrick Schincariol", "Apress", 2013, book_1.BookBinding.PAPERBACK, 508, 54.99),
    new book_1.Book("1782176462", "Mastering JavaServer Faces 2.2", "Anghel Leonard", "Packt Publishing - ebooks Account", 2014, book_1.BookBinding.PAPERBACK, 501, 59.99),
    new book_1.Book("0133795748", "Core JavaServer Faces (4th Edition) (Core Series)", "David Geary, Cay S. Horstmann, Marty Hall", "Prentice Hall", 2015, book_1.BookBinding.PAPERBACK, 672, 59.99),
    new book_1.Book("1449365116", "Java Web Services: Up and Running", "Martin Kalin", "O'Reilly Media", 2013, book_1.BookBinding.PAPERBACK, 360, 39.99),
    new book_1.Book("144936134X", "RESTful Java with JAX-RS 2.0", "Bill Burke", "O'Reilly Media", 2013, book_1.BookBinding.PAPERBACK, 392, 39.99)
];
//# sourceMappingURL=book-data.js.map