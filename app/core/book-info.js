"use strict";
var BookInfo = (function () {
    function BookInfo(isbn, title, authors, price) {
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.price = price;
    }
    return BookInfo;
}());
exports.BookInfo = BookInfo;
//# sourceMappingURL=book-info.js.map