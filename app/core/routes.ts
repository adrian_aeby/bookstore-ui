import {CatalogComponent} from '../catalog/catalog.component';
import {HomeComponent} from './home.component';
import {BookDetailsComponent} from '../catalog/book-details.component';
import {ShoppingCartComponent} from '../shopping-cart/shopping-cart.component';
import {LoginComponent} from '../forms/login.component';
import {Routes } from '@angular/router';
import {RegistrationComponent} from '../forms/registration.component';
import {ChangePasswordComponent} from '../forms/change-password.component';


export const routes: Routes = [
    {path: 'catalog', component: CatalogComponent},
    {path: 'home', component: HomeComponent},
    {path: 'book-details/:isbn', component: BookDetailsComponent},
    {path: 'shopping-cart', component: ShoppingCartComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'login', component: LoginComponent},
    {path: 'change-password', component: ChangePasswordComponent},
    {path: '**', component: HomeComponent}
];