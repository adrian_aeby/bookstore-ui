"use strict";
var Book = (function () {
    function Book(isbn, title, authors, publisher, publicationYear, binding, numberOfPages, price) {
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.publicationYear = publicationYear;
        this.binding = binding;
        this.numberOfPages = numberOfPages;
        this.price = price;
    }
    return Book;
}());
exports.Book = Book;
(function (BookBinding) {
    BookBinding[BookBinding["HARDCOVER"] = 0] = "HARDCOVER";
    BookBinding[BookBinding["PAPERBACK"] = 1] = "PAPERBACK";
    BookBinding[BookBinding["EBOOK"] = 2] = "EBOOK";
    BookBinding[BookBinding["UNKNOWN"] = 3] = "UNKNOWN";
})(exports.BookBinding || (exports.BookBinding = {}));
var BookBinding = exports.BookBinding;
//# sourceMappingURL=book.js.map