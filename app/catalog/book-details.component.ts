import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Book, BookBinding} from "../core/book";
import {Router} from '@angular/router';
import {CatalogService} from '../services/catalog-service';

@Component({
    selector: 'book-details',
    templateUrl: 'app/catalog/book-details.component.html'
})
export class BookDetailsComponent{

    private book: Book;
    private result: Promise<Book>;
    private hasError: boolean = false;


    constructor(private route: ActivatedRoute, private router: Router, private service: CatalogService) {

        console.log("im Konstruktor von BookDetails");

        let isbn = route.snapshot.params['isbn'];

        console.log("route.toString" + route.toString());
        console.log("route.snapshot.toString" + route.snapshot.toString());
        console.log("route.parent.toString" + route.parent.toString());

        console.log("erhaltene ISBN ist: " + isbn);

        this.result = this.service.findBook(isbn);

        console.log("result ist: " + this.result);

        this.result
            .then((r: Book) => {
                // process result
                this.book = r;
            })
            .catch((error: any) => {
                // handle error
                console.log("Error " + error.stack);
                this.hasError = true;
            });
    }

    public back(): void {
        this.router.navigate((['/catalog']));
    }
}