import {Component, Input} from '@angular/core'
import {Router} from '@angular/router'
import {BookInfo} from "../core/book-info";


@Component({
    selector: 'book-list',
    templateUrl: 'app/catalog/book-list.component.html'
})
export class BookListComponent {

    @Input("bookList-books") bookInfos: BookInfo[];

    constructor(private router: Router) {}
    public select(bookInfo: BookInfo): void {
        this.router.navigate((['/book-details', bookInfo.isbn]));
    }
}