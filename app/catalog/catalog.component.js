"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var catalog_service_1 = require('../services/catalog-service');
var cache_service_1 = require("../services/cache-service");
var CatalogComponent = (function () {
    function CatalogComponent(catalog, cache) {
        this.catalog = catalog;
        this.cache = cache;
        this.books = [];
        console.log("Konstruktor von der CatalogComponente");
        this.bookInfos = this.cache.getBookInfos();
        this.keywords = this.cache.getKeywords();
    }
    /*
    public searchBooks()
    */
    CatalogComponent.prototype.searchBooks = function () {
        var _this = this;
        // this.catalog.asyncSerachBooks(this.keywords[0])
        this.catalog.searchBooks(this.keywords)
            .then(function (result) {
            console.log("erfolgreich");
            console.log("anzahl resultate: " + result.length);
            // TODO AA 17.02.2017: Wenn es kein Resultat gibt ist result[0] ungültig
            console.log("bookinfos: " + result[0].authors);
            _this.bookInfos = result;
            _this.cache.setBookInfos(result);
            _this.cache.setKeywords(_this.keywords);
        })
            .catch(function (error) {
            console.log("Fehler");
        });
    };
    CatalogComponent.prototype.updateTotalPrice = function () {
        for (var _i = 0, _a = this.books; _i < _a.length; _i++) {
            var book = _a[_i];
        }
    };
    CatalogComponent.prototype.selectBook = function (inputBook) {
        this.cache.setBookInfos(this.books);
        console.log("methode selectBook aufgerufen:; Titel ist:" + inputBook.title);
        this.selectedBook = inputBook;
    };
    CatalogComponent.prototype.deselectBook = function () {
        this.selectedBook = null;
    };
    CatalogComponent = __decorate([
        core_1.Component({
            selector: 'catalog',
            templateUrl: 'app/catalog/catalog.component.html'
        }), 
        __metadata('design:paramtypes', [catalog_service_1.CatalogService, cache_service_1.CacheService])
    ], CatalogComponent);
    return CatalogComponent;
}());
exports.CatalogComponent = CatalogComponent;
//# sourceMappingURL=catalog.component.js.map