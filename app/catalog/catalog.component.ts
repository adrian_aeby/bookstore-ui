import {Component} from '@angular/core';
import { Book } from '../core/book';
import { BookInfo } from '../core/book-info';
import {CatalogService} from '../services/catalog-service'
import {CacheService} from "../services/cache-service";



@Component({
    selector: 'catalog',
    templateUrl: 'app/catalog/catalog.component.html'
})
export class CatalogComponent{

    private keywords: string;
    private books: Book[] = [];
    private selectedBook: Book;
    private bookInfos: BookInfo[];


    constructor(private catalog: CatalogService, private cache: CacheService) {
        console.log("Konstruktor von der CatalogComponente");
        this.bookInfos = this.cache.getBookInfos();
        this.keywords = this.cache.getKeywords();
    }


    /*
    public searchBooks()
    */


    public searchBooks(): void{
        // this.catalog.asyncSerachBooks(this.keywords[0])
        this.catalog.searchBooks(this.keywords)
            .then((result) => {

                console.log("erfolgreich");
                console.log("anzahl resultate: " + result.length);
                // TODO AA 17.02.2017: Wenn es kein Resultat gibt ist result[0] ungültig
                console.log("bookinfos: " + result[0].authors );
                this.bookInfos = result;
                this.cache.setBookInfos(result);
                this.cache.setKeywords(this.keywords);
            })
            .catch((error: any) => {
                console.log("Fehler");
            });
    }


    public updateTotalPrice() {

        for(let book of this.books) {
            // TODO mit Items arbeiten...
        }
    }

    public selectBook(inputBook: Book) {


        this.cache.setBookInfos(this.books);
        console.log("methode selectBook aufgerufen:; Titel ist:" + inputBook.title)
        this.selectedBook = inputBook;


    }

    public deselectBook() {
        this.selectedBook = null;
    }

}
