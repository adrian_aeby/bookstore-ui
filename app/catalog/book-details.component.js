"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var router_2 = require('@angular/router');
var catalog_service_1 = require('../services/catalog-service');
var BookDetailsComponent = (function () {
    function BookDetailsComponent(route, router, service) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.service = service;
        this.hasError = false;
        console.log("im Konstruktor von BookDetails");
        var isbn = route.snapshot.params['isbn'];
        console.log("route.toString" + route.toString());
        console.log("route.snapshot.toString" + route.snapshot.toString());
        console.log("route.parent.toString" + route.parent.toString());
        console.log("erhaltene ISBN ist: " + isbn);
        this.result = this.service.findBook(isbn);
        console.log("result ist: " + this.result);
        this.result
            .then(function (r) {
            // process result
            _this.book = r;
        })
            .catch(function (error) {
            // handle error
            console.log("Error " + error.stack);
            _this.hasError = true;
        });
    }
    BookDetailsComponent.prototype.back = function () {
        this.router.navigate((['/catalog']));
    };
    BookDetailsComponent = __decorate([
        core_1.Component({
            selector: 'book-details',
            templateUrl: 'app/catalog/book-details.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_2.Router, catalog_service_1.CatalogService])
    ], BookDetailsComponent);
    return BookDetailsComponent;
}());
exports.BookDetailsComponent = BookDetailsComponent;
//# sourceMappingURL=book-details.component.js.map