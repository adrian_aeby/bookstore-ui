"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var BookListComponent = (function () {
    function BookListComponent(router) {
        this.router = router;
    }
    BookListComponent.prototype.select = function (bookInfo) {
        this.router.navigate((['/book-details', bookInfo.isbn]));
    };
    __decorate([
        core_1.Input("bookList-books"), 
        __metadata('design:type', Array)
    ], BookListComponent.prototype, "bookInfos", void 0);
    BookListComponent = __decorate([
        core_1.Component({
            selector: 'book-list',
            templateUrl: 'app/catalog/book-list.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], BookListComponent);
    return BookListComponent;
}());
exports.BookListComponent = BookListComponent;
//# sourceMappingURL=book-list.component.js.map