"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var book_data_1 = require('../core/book-data');
var ShoppingCartComponent = (function () {
    function ShoppingCartComponent() {
        this.items = [];
        this.totalPrice = 0.0;
    }
    ShoppingCartComponent.prototype.ngOnInit = function () {
        for (var n = 0; n < 5; n++) {
            this.addBook(book_data_1.BOOK_DATA[this.random(book_data_1.BOOK_DATA.length)]);
        }
    };
    ShoppingCartComponent.prototype.addBook = function (book) {
        console.log("Adding book " + book);
        var i = this.findItem(book.isbn);
        if (i >= 0) {
            this.items[i].quantity++;
        }
        else {
            this.items.push(new ShoppingCartItem(book, 1));
        }
        this.updateTotalPrice();
    };
    ShoppingCartComponent.prototype.removeBook = function (book) {
        console.log("Removing book " + book);
        var i = this.findItem(book.isbn);
        if (i >= 0) {
            this.items.splice(i, 1);
        }
        this.updateTotalPrice();
    };
    ShoppingCartComponent.prototype.updateTotalPrice = function () {
        var _this = this;
        this.totalPrice = 0.0;
        this.items.forEach(function (item) { return _this.totalPrice += item.book.price * item.quantity; });
    };
    ShoppingCartComponent.prototype.findItem = function (isbn) {
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].book.isbn === isbn) {
                return i;
            }
        }
        return -1;
    };
    ShoppingCartComponent.prototype.random = function (max) {
        return Math.floor(max * Math.random());
    };
    ShoppingCartComponent = __decorate([
        core_1.Component({
            selector: 'shopping-cart',
            templateUrl: 'app/shopping-cart/shopping-cart.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ShoppingCartComponent);
    return ShoppingCartComponent;
}());
exports.ShoppingCartComponent = ShoppingCartComponent;
var ShoppingCartItem = (function () {
    function ShoppingCartItem(book, quantity) {
        this.book = book;
        this.quantity = quantity;
    }
    return ShoppingCartItem;
}());
exports.ShoppingCartItem = ShoppingCartItem;
//# sourceMappingURL=shopping-cart.component.js.map