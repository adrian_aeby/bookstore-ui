import { Component, OnInit } from '@angular/core';
import { Book } from '../core/book';
import { BOOK_DATA } from '../core/book-data';

@Component({
	selector: 'shopping-cart',
	templateUrl: 'app/shopping-cart/shopping-cart.component.html'
})
export class ShoppingCartComponent implements OnInit {

	private items: ShoppingCartItem[] = [];
	private totalPrice: number = 0.0;

	ngOnInit() {
		for (let n = 0; n < 5; n++) {
			this.addBook(BOOK_DATA[this.random(BOOK_DATA.length)]);
		}
	}

	public addBook(book: Book): void {
		console.log("Adding book " + book);
		let i = this.findItem(book.isbn);
		if (i >= 0) {
			this.items[i].quantity++
		} else {
			this.items.push(new ShoppingCartItem(book, 1));
		}
		this.updateTotalPrice();
	}

	public removeBook(book: Book): void {
		console.log("Removing book " + book);
		let i = this.findItem(book.isbn);
		if (i >= 0) {
			this.items.splice(i, 1);
		}
		this.updateTotalPrice();
	}

	public updateTotalPrice(): void {
		this.totalPrice = 0.0;
		this.items.forEach(item => this.totalPrice += item.book.price * item.quantity);
	}

	private findItem(isbn: string): number {
		for (let i = 0; i < this.items.length; i++) {
			if (this.items[i].book.isbn === isbn) {
				return i;
			}
		}
		return -1;
	}

	private random(max: number): number {
		return Math.floor(max * Math.random());
	}
}

export class ShoppingCartItem {
	constructor(readonly book: Book, public quantity: number) {
	}
}
