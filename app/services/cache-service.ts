import { BookInfo } from '../core/book-info';

export class CacheService {

    private bookInfos: BookInfo[];
    private keywords: string;

    public setKeywords(words: string) {
        this.keywords = words;
    }

    public  getKeywords() : string {
        return this.keywords;
    }

    public setBookInfos(input: BookInfo[]): void {

        console.log("setBooks ist aufgerufen");

        this.bookInfos = input;
    }

    public getBookInfos() : BookInfo[] {

        console.log("getBooks ist aufgerufen");
        return this.bookInfos;
    }

}
