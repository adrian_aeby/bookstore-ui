"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/Rx');
var RegistrationService = (function () {
    function RegistrationService(http) {
        this.http = http;
        this.baseUrl = "http://distsys.ch:8080/bookstore/rest/registrations";
        this.wrongLogin = false;
        this.authenticated = false;
    }
    RegistrationService.prototype.register = function (registration) {
        var url = this.baseUrl;
        var headers = new http_1.Headers({ "Content-Type": "application/json" });
        console.log("registration:" + JSON.stringify(registration));
        return this.http.post(url, registration, { headers: headers }).toPromise()
            .then(function (response) {
            console.log("RegistrationService.register.then:" + response);
            console.log("Status:" + response.status);
            return response.status;
        })
            .catch(function (error) {
            console.error("RegistrationService.register.catch:" + error);
            console.log("Status:" + error.status);
            return Promise.reject(error);
        });
    };
    RegistrationService.prototype.authenticate = function (email, password) {
        var _this = this;
        // http://distsys.ch:8080/bookstore/rest/registrations/trash%40sensemail.ch
        this.email = email;
        this.password = password;
        var url = this.baseUrl + "/" + email;
        var headers = new http_1.Headers({ "password": password });
        // return this.http.get(url, headers)
        //     .catch((response: Response) => {
        //         return Observable.throw(response.json());
        //     })
        var result = this.http.get(url, headers).toPromise()
            .then(function (response) {
            console.log("RegistrationService.authenticate.then:" + response);
            return response.json();
        })
            .catch(function (error) {
            console.error("RegistrationService.authenticate.catch:" + error);
            return Promise.reject(error);
        });
        console.log("result ist: " + result);
        result
            .then(function (result) {
            _this.authenticated = true;
            _this.wrongLogin = false;
            _this.customerNo = result;
        })
            .catch(function (error) {
            console.log("Error: " + error);
            switch (error.status) {
                case 401:
                case 409:
                    _this.wrongLogin = true;
                    break;
                default:
                    _this.wrongLogin = false;
            }
            _this.authenticated = false;
        });
    };
    RegistrationService.prototype.clear = function () {
        this.wrongLogin = false;
        this.authenticated = false;
        this.customerNo = null;
        this.email = null;
        this.password = null;
    };
    RegistrationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RegistrationService);
    return RegistrationService;
}());
exports.RegistrationService = RegistrationService;
//# sourceMappingURL=registration.service.js.map