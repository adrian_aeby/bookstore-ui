import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {Registration} from "../forms/dtos/registration";
import 'rxjs/Rx';

@Injectable()
export class RegistrationService {
    private baseUrl = "http://distsys.ch:8080/bookstore/rest/registrations";

    public wrongLogin: boolean = false;
    public authenticated: boolean = false;
    public customerNo: string;

    public email: string;
    public password: string;

    constructor(private http: Http) {}

    public register(registration: Registration) {
        const url = this.baseUrl;
        const headers = new Headers({"Content-Type": "application/json"});
        console.log("registration:"+JSON.stringify(registration));
        return this.http.post(url, registration, {headers: headers}).toPromise()
            .then(response => {
                console.log("RegistrationService.register.then:"+response);
                console.log("Status:"+response.status);
                return response.status;
            })
            .catch(error => {
                console.error("RegistrationService.register.catch:"+error);
                console.log("Status:"+error.status);
                return Promise.reject(error);
            })
        ;
    }

    public authenticate(email: string, password: string) {
        // http://distsys.ch:8080/bookstore/rest/registrations/trash%40sensemail.ch
        this.email = email;
        this.password = password;
        const url = this.baseUrl + "/" + email;
        const headers = new Headers({"password": password});
        // return this.http.get(url, headers)
        //     .catch((response: Response) => {
        //         return Observable.throw(response.json());
        //     })
        const result = this.http.get(url, headers).toPromise()
            .then(response => {
                console.log("RegistrationService.authenticate.then:"+response)
                return response.json();
            })
            .catch(error => {
                console.error("RegistrationService.authenticate.catch:"+error)
                return Promise.reject(error);
            });

        console.log("result ist: " + result);

        result
            .then(result => {
                this.authenticated = true;
                this.wrongLogin = false;
                this.customerNo = result;
            })
            .catch(error => {
                console.log("Error: " + error);
                switch (error.status) {
                    case 401:
                    case 409:
                        this.wrongLogin = true;
                        break;
                    default:
                        this.wrongLogin = false;
                }
                this.authenticated = false;
            })

    }

    clear() {
        this.wrongLogin = false;
        this.authenticated = false;
        this.customerNo = null;
        this.email = null;
        this.password = null;
    }
}
