import { Injectable } from '@angular/core';
import { Book } from '../core/book';
import { BOOK_DATA} from '../core/book-data';
import { Http } from '@angular/http';
import { Headers} from '@angular/http';
import "rxjs/add/operator/toPromise";
import { BookInfo} from '../core/book-info';

@Injectable()
export class CatalogService {

    private baseUrl = "http://distsys.ch:8080/bookstore/rest/books";
// eventuell static


    constructor(private http: Http) {}

    public findBook(isbn: string): Promise<Book> {
        let url = this.baseUrl + "/" + isbn;
        let headers = new Headers({"Accept": "application/json"});
        return this.http.get(url, {headers: headers}).toPromise()
                .then(response => response.json() as Book)
                .catch(error => this.handleError(error))
                /*
                {
                    //console.error("-----------CatalogService: " + error);
                    return Promise.reject(error);
                });
                */
    }

    /*
    // Promise<Book[]>
    public searchBooks(keywords: string): Book[] {
        console.log("Service 'serachBooks aufgerufen'");

        let books: Book[] = [];

        for (let book of BOOK_DATA) {
            if (book.title.includes(keywords)){
                console.log("titel ist: " + book.title);
                books.push(book);
            }
        }

        return books;
    }
    */

    private handleError(error:any): Promise<any> {
        console.error("Error im CatalogService: " + error);
        return Promise.reject(error);
    }


    public searchBooks(keywords: string): Promise<BookInfo[]> {
        let url = this.baseUrl + "?keywords=" + keywords;
        console.log("übergeben wird: " + keywords);
        let headers = new Headers({"Accept": "application/json"});
        return this.http.get(url, {headers: headers}).toPromise()
            .then(response => response.json() as BookInfo[])
            .catch(error => {
                return Promise.reject(error);
            });
    }

    public asyncSerachBooks(keywords: string): Promise<Book[]> {
        return new Promise<Book[]>((resolve, reject) => {
            console.log("in der async Methode");

            let books: Book[] = [];

            for (let book of BOOK_DATA) {
                if (book.title.includes(keywords)){
                    console.log("titel ist: " + book.title);
                    books.push(book);
                }
            }

            if (books == undefined || books.length == 0) {
                reject("Fehler da kein Buch gefunden");
                return;
            }




            setTimeout(function() {
             resolve(books);
            }, 1000);

        });
    }
}


// wenn keine auf leeren string prüfen    return Promise.reject("missing keyword);