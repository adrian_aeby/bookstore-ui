import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {CustomerInfo} from "../forms/dtos/customer-info";
import {Customer} from "../forms/dtos/customer";
@Injectable()
export class CustomerService {
    private baseUrl = "http://distsys.ch:8080/bookstore/rest/customers";

    public customer: Customer;
    public customerFound: boolean;
    public customerNo: string;

    constructor(private http: Http) {}

    public searchCustomer(name: string) {
        const url = this.baseUrl + "?name=" + name;
        const headers = new Headers({"Accept": "application/json"});
        console.log("searchCustomer:"+name);
        return this.http.get(url, {headers: headers}).toPromise()
            .then(response => response.json() as CustomerInfo[])
            .catch(error => Promise.reject(error))
        ;
    }

    public getCustomer(customerNo: string) {
        this.customerNo = customerNo;
        const url = this.baseUrl + "/" + customerNo;
        const headers = new Headers({"Accept": "application/json"});
        console.log("getCustomer:"+customerNo);
        const result =  this.http.get(url, {headers: headers}).toPromise()
            .then(response => response.json() as Customer)
            .catch(error => Promise.reject(error))
            ;

        result
            .then(result => {
                this.customer = result;
                this.customerFound = true;
            })
            .catch(error => {
                console.error("Fehler beim Aufruf des REST-Services: " + error);
                this.customer = null;
                this.customerFound = false;
            })
    }

    clear() {
        this.customer = null;
        this.customerFound = null;
        this.customerNo = null;
    }
}
