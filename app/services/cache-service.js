"use strict";
var CacheService = (function () {
    function CacheService() {
    }
    CacheService.prototype.setKeywords = function (words) {
        this.keywords = words;
    };
    CacheService.prototype.getKeywords = function () {
        return this.keywords;
    };
    CacheService.prototype.setBookInfos = function (input) {
        console.log("setBooks ist aufgerufen");
        this.bookInfos = input;
    };
    CacheService.prototype.getBookInfos = function () {
        console.log("getBooks ist aufgerufen");
        return this.bookInfos;
    };
    return CacheService;
}());
exports.CacheService = CacheService;
//# sourceMappingURL=cache-service.js.map