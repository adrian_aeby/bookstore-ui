"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var book_data_1 = require('../core/book-data');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
require("rxjs/add/operator/toPromise");
var CatalogService = (function () {
    // eventuell static
    function CatalogService(http) {
        this.http = http;
        this.baseUrl = "http://distsys.ch:8080/bookstore/rest/books";
    }
    CatalogService.prototype.findBook = function (isbn) {
        var _this = this;
        var url = this.baseUrl + "/" + isbn;
        var headers = new http_2.Headers({ "Accept": "application/json" });
        return this.http.get(url, { headers: headers }).toPromise()
            .then(function (response) { return response.json(); })
            .catch(function (error) { return _this.handleError(error); });
        /*
        {
            //console.error("-----------CatalogService: " + error);
            return Promise.reject(error);
        });
        */
    };
    /*
    // Promise<Book[]>
    public searchBooks(keywords: string): Book[] {
        console.log("Service 'serachBooks aufgerufen'");

        let books: Book[] = [];

        for (let book of BOOK_DATA) {
            if (book.title.includes(keywords)){
                console.log("titel ist: " + book.title);
                books.push(book);
            }
        }

        return books;
    }
    */
    CatalogService.prototype.handleError = function (error) {
        console.error("Error im CatalogService: " + error);
        return Promise.reject(error);
    };
    CatalogService.prototype.searchBooks = function (keywords) {
        var url = this.baseUrl + "?keywords=" + keywords;
        console.log("übergeben wird: " + keywords);
        var headers = new http_2.Headers({ "Accept": "application/json" });
        return this.http.get(url, { headers: headers }).toPromise()
            .then(function (response) { return response.json(); })
            .catch(function (error) {
            return Promise.reject(error);
        });
    };
    CatalogService.prototype.asyncSerachBooks = function (keywords) {
        return new Promise(function (resolve, reject) {
            console.log("in der async Methode");
            var books = [];
            for (var _i = 0, BOOK_DATA_1 = book_data_1.BOOK_DATA; _i < BOOK_DATA_1.length; _i++) {
                var book = BOOK_DATA_1[_i];
                if (book.title.includes(keywords)) {
                    console.log("titel ist: " + book.title);
                    books.push(book);
                }
            }
            if (books == undefined || books.length == 0) {
                reject("Fehler da kein Buch gefunden");
                return;
            }
            setTimeout(function () {
                resolve(books);
            }, 1000);
        });
    };
    CatalogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CatalogService);
    return CatalogService;
}());
exports.CatalogService = CatalogService;
// wenn keine auf leeren string prüfen    return Promise.reject("missing keyword); 
//# sourceMappingURL=catalog-service.js.map