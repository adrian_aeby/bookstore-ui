"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var CustomerService = (function () {
    function CustomerService(http) {
        this.http = http;
        this.baseUrl = "http://distsys.ch:8080/bookstore/rest/customers";
    }
    CustomerService.prototype.searchCustomer = function (name) {
        var url = this.baseUrl + "?name=" + name;
        var headers = new http_1.Headers({ "Accept": "application/json" });
        console.log("searchCustomer:" + name);
        return this.http.get(url, { headers: headers }).toPromise()
            .then(function (response) { return response.json(); })
            .catch(function (error) { return Promise.reject(error); });
    };
    CustomerService.prototype.getCustomer = function (customerNo) {
        var _this = this;
        this.customerNo = customerNo;
        var url = this.baseUrl + "/" + customerNo;
        var headers = new http_1.Headers({ "Accept": "application/json" });
        console.log("getCustomer:" + customerNo);
        var result = this.http.get(url, { headers: headers }).toPromise()
            .then(function (response) { return response.json(); })
            .catch(function (error) { return Promise.reject(error); });
        result
            .then(function (result) {
            _this.customer = result;
            _this.customerFound = true;
        })
            .catch(function (error) {
            console.error("Fehler beim Aufruf des REST-Services: " + error);
            _this.customer = null;
            _this.customerFound = false;
        });
    };
    CustomerService.prototype.clear = function () {
        this.customer = null;
        this.customerFound = null;
        this.customerNo = null;
    };
    CustomerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CustomerService);
    return CustomerService;
}());
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map