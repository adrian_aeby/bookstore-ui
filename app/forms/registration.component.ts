///<reference path="../services/registration.service.ts"/>
import {Component} from '@angular/core';
import {NgForm} from '@angular/forms'
import {CreditCard} from "./dtos/credit-card";
import {CreditType} from "./dtos/credit-type";
import {Router} from '@angular/router'
import {RegistrationService} from "../services/registration.service";
import {Customer} from "./dtos/customer";
import {Address} from "./dtos/address";
import {Registration} from "./dtos/registration";
import {CustomerService} from "../services/customer.service";

@Component({
    selector: 'registration',
    templateUrl: 'app/forms/registration.component.html',
    styles: [`
        input.ng-touched.ng-invalid.ng-dirty {
            border: 1px solid red;
        }  
    `]
})
export class RegistrationComponent {

    public creditCardTypes: CreditType[]  = [];
    public creditCard: CreditCard = new CreditCard("","","","");
    public minYear: number = 2017;
    public maxYear: number = 2027;
    public name = "Frédéric";

    private userData: any;
    private registrationSuccessful: boolean = false;
    private errorType: number;

    private registration: Registration;

    constructor(
        private router: Router,
        private registrationService: RegistrationService,
        private customerService: CustomerService
    ) {
        let type1 = new CreditType("MASTERCARD", "MasterCard");
        let type2 = new CreditType("VISA", "Visa");

        this.creditCardTypes.push(type1);
        this.creditCardTypes.push(type2);

        this.registration = new Registration(customerService.customer, registrationService.password);
    }

    // TODO min un max berechnen
    // TODO noch regex bei Jahr und Monat
    // TODO Dropdown für Land ?


    onSubmit(form: NgForm) {
        this.errorType = 0;
        console.log(form);

        let street = form.value.address.street;
        let city = form.value.address.city;
        let postalCode = form.value.address.postalCode;
        let country = form.value.address.country;
        console.log('street:'+street);
        console.log('city:'+city);
        console.log('postalCode:'+postalCode);
        console.log('country:'+country);
        let address: Address = new Address(street, city, postalCode, country);

        let expirationMonth = form.value.creditCard.month;
        let expirationYear = form.value.creditCard.year;
        let number = form.value.creditCard.cardNumber;
        let type = form.value.creditCard.type;
        console.log('expirationMonth:'+expirationMonth);
        console.log('expirationYear:'+expirationYear);
        console.log('number:'+number);
        console.log('type:'+type);
        let creditCart = new CreditCard(type, number, expirationMonth, expirationYear);

        let firstName = form.value.userData.firstName;
        let lastName = form.value.userData.lastName;
        let email = form.value.userData.email;
        console.log('firstName:'+firstName);
        console.log('lastName :'+lastName);
        console.log('email    :'+email);
        let customer: Customer = new Customer(firstName, lastName, email, address, creditCart);

        let password = form.value.userData.password;
        console.log('password :'+password);
        this.registration = new Registration(customer, password);

        this.result = this.registrationService.register(this.registration);
        console.log(this.result);
        this.result
            .then(status => {
                console.log("status:"+status);
                this.registrationSuccessful = true;
            })
            .catch(error => {
                console.log("error:"+error);
                this.registrationSuccessful = false;
                if (error.status == 409) {
                    this.errorType = 1;
                } else {
                    this.errorType = 2;
                }
            })
    }
    private result: Promise<string>;
    onCancel() {
        this.router.navigate(['/home']);
    }

}