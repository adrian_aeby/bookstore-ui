import {Component} from '@angular/core';
import {NgForm} from '@angular/forms'
import {RegistrationService} from "../services/registration.service";

@Component({
    selector: 'login',
    templateUrl: 'app/forms/login.component.html',
    styles: [`
        input.ng-touched.ng-invalid.ng-dirty {
            border: 1px solid red;
        }   
    `]
})
export class LoginComponent {
    public wrongLogin: boolean;
    public authenticated: boolean;
    public customerNo: string;

    constructor(private registrationService: RegistrationService) {
        this.authenticated  = this.registrationService.authenticated;
        this.wrongLogin = this.registrationService.wrongLogin;
        this.customerNo = this.registrationService.customerNo;
    }

    onSubmit(form: NgForm) {
        const email = form.form.get("email").value;
        const password = form.form.get("password").value;

        console.log("email ist: " + email);
        console.log("passwort ist: " + password);

        // TODO: sobald klar ist wieso der REST Service immer 401 liefert wieder entfernen
        // Workaround start
        if (email == "OK") {
            this.registrationService.authenticated = true;
            this.registrationService.wrongLogin = false;
            this.registrationService.customerNo = "2";
        } else {
        // Workaround ende
            this.registrationService.authenticate(email, password);
        }
        this.authenticated  = this.registrationService.authenticated;
        this.wrongLogin = this.registrationService.wrongLogin;
        this.customerNo = this.registrationService.customerNo;
    }


    //TODO: wollen wir die E-Mail Adresse prüfen? --> Nein, wir nur bei der Registration geprüft!
}