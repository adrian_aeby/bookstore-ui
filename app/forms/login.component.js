"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var registration_service_1 = require("../services/registration.service");
var LoginComponent = (function () {
    function LoginComponent(registrationService) {
        this.registrationService = registrationService;
        this.authenticated = this.registrationService.authenticated;
        this.wrongLogin = this.registrationService.wrongLogin;
        this.customerNo = this.registrationService.customerNo;
    }
    LoginComponent.prototype.onSubmit = function (form) {
        var email = form.form.get("email").value;
        var password = form.form.get("password").value;
        console.log("email ist: " + email);
        console.log("passwort ist: " + password);
        // TODO: sobald klar ist wieso der REST Service immer 401 liefert wieder entfernen
        // Workaround start
        if (email == "OK") {
            this.registrationService.authenticated = true;
            this.registrationService.wrongLogin = false;
            this.registrationService.customerNo = "2";
        }
        else {
            // Workaround ende
            this.registrationService.authenticate(email, password);
        }
        this.authenticated = this.registrationService.authenticated;
        this.wrongLogin = this.registrationService.wrongLogin;
        this.customerNo = this.registrationService.customerNo;
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'app/forms/login.component.html',
            styles: ["\n        input.ng-touched.ng-invalid.ng-dirty {\n            border: 1px solid red;\n        }   \n    "]
        }), 
        __metadata('design:paramtypes', [registration_service_1.RegistrationService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map