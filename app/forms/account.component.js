"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var customer_service_1 = require("../services/customer.service");
var registration_service_1 = require("../services/registration.service");
var AccountComponent = (function () {
    function AccountComponent(customerService, registrationServcie) {
        this.customerService = customerService;
        this.registrationServcie = registrationServcie;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.customerService.getCustomer(this.customerNo);
        this.customerFound = this.customerService.customerFound;
        this.customer = this.customerService.customer;
    };
    AccountComponent.prototype.signOut = function () {
        console.log("Kunde hat sich abgemeldet.");
        this.customerService.clear();
        this.registrationServcie.clear();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], AccountComponent.prototype, "customerNo", void 0);
    AccountComponent = __decorate([
        core_1.Component({
            selector: 'account',
            templateUrl: 'app/forms/account.component.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, registration_service_1.RegistrationService])
    ], AccountComponent);
    return AccountComponent;
}());
exports.AccountComponent = AccountComponent;
//# sourceMappingURL=account.component.js.map