import {Component, OnInit, Input} from '@angular/core';
import {CustomerService} from "../services/customer.service";
import {Customer} from "./dtos/customer";
import {RegistrationService} from "../services/registration.service";

@Component({
    selector: 'account',
    templateUrl: 'app/forms/account.component.html'
})

export class AccountComponent implements OnInit {

    @Input() private customerNo: string;
    private customer: Customer;
    private customerFound: boolean;

    constructor(private customerService: CustomerService, private registrationServcie: RegistrationService) {}

    ngOnInit(): void {
        this.customerService.getCustomer(this.customerNo);
        this.customerFound = this.customerService.customerFound;
        this.customer = this.customerService.customer;
    }

    signOut() {
        console.log("Kunde hat sich abgemeldet.")
        this.customerService.clear();
        this.registrationServcie.clear();
    }
}