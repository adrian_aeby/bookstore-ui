"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
///<reference path="../services/registration.service.ts"/>
var core_1 = require('@angular/core');
var credit_card_1 = require("./dtos/credit-card");
var credit_type_1 = require("./dtos/credit-type");
var router_1 = require('@angular/router');
var registration_service_1 = require("../services/registration.service");
var customer_1 = require("./dtos/customer");
var address_1 = require("./dtos/address");
var registration_1 = require("./dtos/registration");
var customer_service_1 = require("../services/customer.service");
var RegistrationComponent = (function () {
    function RegistrationComponent(router, registrationService, customerService) {
        this.router = router;
        this.registrationService = registrationService;
        this.customerService = customerService;
        this.creditCardTypes = [];
        this.creditCard = new credit_card_1.CreditCard("", "", "", "");
        this.minYear = 2017;
        this.maxYear = 2027;
        this.name = "Frédéric";
        this.registrationSuccessful = false;
        var type1 = new credit_type_1.CreditType("MASTERCARD", "MasterCard");
        var type2 = new credit_type_1.CreditType("VISA", "Visa");
        this.creditCardTypes.push(type1);
        this.creditCardTypes.push(type2);
        this.registration = new registration_1.Registration(customerService.customer, registrationService.password);
    }
    // TODO min un max berechnen
    // TODO noch regex bei Jahr und Monat
    // TODO Dropdown für Land ?
    RegistrationComponent.prototype.onSubmit = function (form) {
        var _this = this;
        this.errorType = 0;
        console.log(form);
        var street = form.value.address.street;
        var city = form.value.address.city;
        var postalCode = form.value.address.postalCode;
        var country = form.value.address.country;
        console.log('street:' + street);
        console.log('city:' + city);
        console.log('postalCode:' + postalCode);
        console.log('country:' + country);
        var address = new address_1.Address(street, city, postalCode, country);
        var expirationMonth = form.value.creditCard.month;
        var expirationYear = form.value.creditCard.year;
        var number = form.value.creditCard.cardNumber;
        var type = form.value.creditCard.type;
        console.log('expirationMonth:' + expirationMonth);
        console.log('expirationYear:' + expirationYear);
        console.log('number:' + number);
        console.log('type:' + type);
        var creditCart = new credit_card_1.CreditCard(type, number, expirationMonth, expirationYear);
        var firstName = form.value.userData.firstName;
        var lastName = form.value.userData.lastName;
        var email = form.value.userData.email;
        console.log('firstName:' + firstName);
        console.log('lastName :' + lastName);
        console.log('email    :' + email);
        var customer = new customer_1.Customer(firstName, lastName, email, address, creditCart);
        var password = form.value.userData.password;
        console.log('password :' + password);
        this.registration = new registration_1.Registration(customer, password);
        this.result = this.registrationService.register(this.registration);
        console.log(this.result);
        this.result
            .then(function (status) {
            console.log("status:" + status);
            _this.registrationSuccessful = true;
        })
            .catch(function (error) {
            console.log("error:" + error);
            _this.registrationSuccessful = false;
            if (error.status == 409) {
                _this.errorType = 1;
            }
            else {
                _this.errorType = 2;
            }
        });
    };
    RegistrationComponent.prototype.onCancel = function () {
        this.router.navigate(['/home']);
    };
    RegistrationComponent = __decorate([
        core_1.Component({
            selector: 'registration',
            templateUrl: 'app/forms/registration.component.html',
            styles: ["\n        input.ng-touched.ng-invalid.ng-dirty {\n            border: 1px solid red;\n        }  \n    "]
        }), 
        __metadata('design:paramtypes', [router_1.Router, registration_service_1.RegistrationService, customer_service_1.CustomerService])
    ], RegistrationComponent);
    return RegistrationComponent;
}());
exports.RegistrationComponent = RegistrationComponent;
//# sourceMappingURL=registration.component.js.map