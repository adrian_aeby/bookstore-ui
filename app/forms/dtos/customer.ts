import {Address} from "./address";
import {CreditCard} from "./credit-card";
export class Customer {
    constructor(
        public firstName: string,
        public lastName: string,
        public email: string,
        public address: Address,
        public creditCard: CreditCard
    ) { }
}
