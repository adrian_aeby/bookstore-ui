"use strict";
var CreditType = (function () {
    function CreditType(value, label) {
        this.value = value;
        this.label = label;
    }
    return CreditType;
}());
exports.CreditType = CreditType;
//# sourceMappingURL=credit-type.js.map