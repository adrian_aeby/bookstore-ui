"use strict";
var Address = (function () {
    function Address(street, city, postalCode, country) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
    }
    return Address;
}());
exports.Address = Address;
//# sourceMappingURL=address.js.map