export class CustomerInfo {
    constructor(
        public number: number,
        public firstName: string,
        public lastName: string,
        public email: string
    ) { }
}
