"use strict";
var Customer = (function () {
    function Customer(firstName, lastName, email, address, creditCard) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.creditCard = creditCard;
    }
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.js.map