
export class CreditCard {

    constructor(
        public type: string,
        public number: string,
        public expirationMonth: string,
        public expirationYear: string
    ) {}
}