"use strict";
var CustomerInfo = (function () {
    function CustomerInfo(number, firstName, lastName, email) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    return CustomerInfo;
}());
exports.CustomerInfo = CustomerInfo;
//# sourceMappingURL=customer-info.js.map