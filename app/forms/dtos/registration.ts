import {Customer} from "./customer";
export class Registration {
    constructor(
        public customer: Customer,
        public password: String
    ) { }
}