"use strict";
var CreditCard = (function () {
    function CreditCard(type, number, expirationMonth, expirationYear) {
        this.type = type;
        this.number = number;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
    }
    return CreditCard;
}());
exports.CreditCard = CreditCard;
//# sourceMappingURL=credit-card.js.map