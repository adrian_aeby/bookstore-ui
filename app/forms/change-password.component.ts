import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";


@Component({
    selector: 'change-password',
    templateUrl: 'app/forms/change-password.component.html',
    styles: [`
        input.ng-touched.ng-invalid.ng-dirty {
            border: 1px solid red;
        }   
    `]
})

export class ChangePasswordComponent{

    constructor(private router: Router) {}

    onSubmit(form: NgForm) {
        console.log(form);
    }

    //  this.router.navigate((['/book-details', book.isbn]));

}